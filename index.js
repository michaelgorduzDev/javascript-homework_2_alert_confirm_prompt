let yourName = prompt("Enter your name");

if (yourName === null) {
    console.log("Thank you for using our service!");
} else {

    while (yourName.length === 0) {
        yourName = prompt("Enter your name", yourName);
    }

    let age = prompt("Enter your age");
    if (age === null) {
        console.log("Thank you for using our service!");
    } else {
        while (age.length === 0 || isNaN(age) === true || age == 0) {
            age = prompt("Enter your age", age);
        }

        if (age < 18) {
            alert("You are not allowed to visit this website");
        } else if (age >= 18 && age <= 22) {

            if (window.confirm("Are you sure you want to continue?")) {
                alert("Welcome, " + yourName);
            } else {
                alert("You are not allowed to visit this website");
            }
        } else {
            alert("Welcome, " + yourName);
        }

    }
}
